import math
import matplotlib.pyplot as plt
import random
def f(x):
	return math.sin(x)
x_points = []
y_points = []
a = float(input ("Inserire estremo inferiore: "))
b = float(input("Inserire estremo superiore: "))
precision = float(input("Inserire precisione: "))
value = a
while(value <= b):
	x_points.append(value)
	value += precision
print(x_points)
for x in x_points:
	y_points.append(f(x))
print(y_points)
plt.plot(x_points , y_points,'ro')
plt.show()

